package com.example.demo.constant;

import org.springframework.http.HttpStatus;


public enum ReturnCode {

    TXN000("TXN000", "Transaction success.", HttpStatus.OK),
    TXN001("TXN001", "Transaction Create success", HttpStatus.CREATED)
    ;

    private final String code;
    private final String message;
    private final HttpStatus httpStatus;

    ReturnCode(String code, String message, HttpStatus httpStatus) {

        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
